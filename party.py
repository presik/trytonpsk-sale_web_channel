# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    id_reference = fields.Char('ID Reference Api', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
