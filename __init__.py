# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import party
from . import web_channel
from . import mercado_libre
from . import shopify
from . import rappi
from . import api_log
from . import ir
# from . import routes
from . import shop
from . import web


# __all__ = ['register', 'routes']
__all__ = ['register']


def register():
    Pool.register(
        # web_channel.SaleWebChannel,
        web.Shop,
        # shopify.Shopify,
        sale.Sale,
        sale.SaleForChannelStart,
        web.SynchronizeChannelOrdersStart,
        web.SynchronizeChannelOrdersDone,
        # web_channel.FinishInvoicesStart,
        party.Party,
        api_log.ApiLog,
        ir.Cron,
        module='sale_web_channel', type_='model')
    Pool.register(
        sale.SaleUploadInvoice,
        sale.SaleForChannel,
        web.SynchronizeChannelOrders,
        # web_channel.FinishInvoices,
        # web_channel.SynchronizeMenuWizard,
        module='sale_web_channel', type_='wizard')
    Pool.register(
        sale.SaleForChannelReport,
        module='sale_web_channel', type_='report')
